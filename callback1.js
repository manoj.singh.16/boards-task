//Problem 1: Write a function that will return a particular board from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const callback1 = (boardsData, boardId, callbackFunction) => {
  setTimeout(() => {
    if (
      Array.isArray(boardsData) &&
      typeof boardId === "string" &&
      typeof callbackFunction === "function"
    ) {
      const particularBoard = boardsData.find((board) => board.id === boardId);

      if (particularBoard !== undefined) {
        callbackFunction(null, particularBoard);
      } else {
        callbackFunction({ message: "Board not found" });
      }
    }
  }, 2 * 1000);
};

module.exports = callback1;
