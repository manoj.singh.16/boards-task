// Problem 2: Write a function that will return a particular list that belongs to a board from the given data in lists.json and then pass control back to the code that called it by using a callback function.

const callback2 = (listData, boardId, callbackFunction) => {
  setTimeout(() => {
    if (
      typeof listData === "object" &&
      typeof boardId === "string" &&
      typeof callbackFunction === "function"
    ) {
      const particularList = listData[boardId];

      if (particularList !== undefined) {
        callbackFunction(null, particularList);
      } else {
        callbackFunction({ message: "list not found" });
      }
    }
  }, 2 * 1000);
};

module.exports = callback2;
