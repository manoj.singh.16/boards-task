// Problem 3: Write a function that will return a particular set of cards that belong to a list from the given data in cards.json and then pass control back to the code that called it by using a callback function.

const callback3 = (cardData, listId, callbackFunction) => {
  setTimeout(() => {
    if (
      typeof cardData === "object" &&
      typeof listId === "string" &&
      typeof callbackFunction === "function"
    ) {
      const particularList = cardData[listId];

      if (particularList !== undefined) {
        callbackFunction(null, particularList);
      } else {
        callbackFunction({ message: "cards not found" });
      }
    }
  }, 2 * 1000);
};

module.exports = callback3;
