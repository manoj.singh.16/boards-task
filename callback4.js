// Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
// board -> lists -> cards for list qwsa221

// mcu453ed -> def453ed -> qwsa221

const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const callback4 = (boardsData, listData, cardData, config) => {
  setTimeout(() => {
    if (Array.isArray(boardsData) && typeof listData === 'object' && typeof cardData === 'object' && typeof config === 'object') {
      const { boardName, listName } = config;

      const board = boardsData.find((board) => board.name === boardName);

      if (board) {
        console.log(board);

        callback2(listData, board.id, (listError, particularList) => {
          if (listError !== null) {
            console.error(listError);
          } else {
            console.log(particularList);

            const list = particularList.find((list) => list.name === listName);

            if (list) {
              callback3(cardData, list.id, (cardError, particularCards) => {
                if (cardError !== null) {
                  console.error(cardError);
                } else {
                  console.log(particularCards);
                }
              });
            } else {
              console.log('List not Found');
            }
          }
        });
      } else {
        console.log('Board not found');
      }
    }
  }, 2 * 1000);
};

module.exports = callback4;
