// Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
// board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously

const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const callback5 = (boardsData, listData, cardData, config) => {
  setTimeout(() => {
    if (Array.isArray(boardsData) && typeof listData === 'object' && typeof cardData === 'object' && typeof config === 'object') {
      const { boardName, listName1, listName2 } = config;

      const board = boardsData.find((board) => board.name === boardName);

      if (board) {
        console.log(board);

        callback2(listData, board.id, (listError, particularList) => {
          if (listError !== null) {
            console.error(listError);
          } else {
            console.log(particularList);

            const list = particularList.filter((list) => list.name === listName1 || list.name === listName2);

            if (list) {
              list.forEach((item) => {
                callback3(cardData, item.id, (cardError1, particularCards) => {
                  if (cardError1 !== null) {
                    console.error(cardError1);
                  } else {
                    console.log(particularCards);
                  }
                });
              });
            } else {
              console.log('List not found');
            }
          }
        });
      } else {
        console.log('Board not found');
      }
    }
  }, 2 * 1000);
};

module.exports = callback5;
