// Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.
// board -> lists -> cards for all lists simultaneously

const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const callback6 = (boardsData, listData, cardData, config) => {
  setTimeout(() => {
    if (Array.isArray(boardsData) && typeof listData === 'object' && typeof cardData === 'object' && typeof config === 'object') {
      const { boardName } = config;

      const board = boardsData.find((board) => board.name === boardName);

      if (board) {
        console.log(board);

        callback2(listData, board.id, (listError, particularList) => {
          if (listError !== null) {
            console.error(listError);
          } else {
            console.log(particularList);

            Object.keys(cardData).forEach((listId) =>
              callback3(cardData, listId, (cardError, particularCards) => {
                if (cardError !== null) {
                  console.error(cardError);
                } else {
                  console.log(particularCards);
                }
              })
            );
          }
        });
      } else {
        console.log('Board not found');
      }
    }
  }, 2 * 1000);
};

module.exports = callback6;
