const callback1 = require("../callback1");
const boardsData = require("../data/boards.json");

const getBoard = (error, data) => {
  if (error !== null) {
    console.error(error);
  } else {
    console.log(data);
  }
};

const boardId = "mcu453ed";

callback1(boardsData, boardId, getBoard);
