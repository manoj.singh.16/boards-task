const callback2 = require("../callback2");
const listData = require("../data/lists.json");

const boardId = "mcu453ed";

const getList = (error, data) => {
  if (error !== null) {
    console.error(error);
  } else {
    console.log(data);
  }
};

callback2(listData, boardId, getList);
