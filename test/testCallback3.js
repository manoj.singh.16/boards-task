const callback3 = require("../callback3");
const cardData = require("../data/cards.json");

const getCards = (error, data) => {
  if (error !== null) {
    console.error(error);
  } else {
    console.log(data);
  }
};

const listId = "qwsa221";

callback3(cardData, listId, getCards);
