const callback4 = require('../callback4');
const boardsData = require('../data/boards.json');
const listData = require('../data/lists.json');
const cardData = require('../data/cards.json');

// board -> lists -> cards for list qwsa221
// mcu453ed -> def453ed -> qwsa221

const config = {
  boardName: 'Thanos',
  listName: 'Mind',
};

callback4(boardsData, listData, cardData, config);
