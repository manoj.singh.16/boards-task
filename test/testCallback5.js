const callback5 = require('../callback5');
const boardsData = require('../data/boards.json');
const listData = require('../data/lists.json');
const cardData = require('../data/cards.json');

// board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously

const config = {
  boardName: 'Thanos',
  listName1: 'Mind',
  listName2: 'Space',
};

callback5(boardsData, listData, cardData, config);
