const callback6 = require("../callback6");
const boardsData = require("../data/boards.json");
const listData = require("../data/lists.json");
const cardData = require("../data/cards.json");

const config = {
  boardName: "Thanos",
};

callback6(boardsData, listData, cardData, config);
